#include <stdio.h>
#include <string.h>
#include "freertos/FreeRTOS.h"
#include "freertos/task.h"
#include "freertos/queue.h"
#include "freertos/event_groups.h"
#include "esp_system.h"
#include "esp_wifi.h"
#include "esp_event.h"
#include "esp_log.h"
#include "nvs_flash.h"
#include "driver/uart.h"
#include "driver/gpio.h"
#include "lwip/err.h"
#include "lwip/sys.h"

#define UART_NUM UART_NUM_1
#define UART_BAUD_RATE 115200
#define TXD_PIN (GPIO_NUM_4) // Pino de TX da UART
#define RXD_PIN (GPIO_NUM_5) // Pino de RX da UART

const char *TAG = "sniffer"; // Tag de LOG

const int maxCh = 13;                // max Channel -> US = 11, EU = 13, Japan = 14
const int RX_BUF_SIZE = 1024;        // Tamanho do buffer de leitura via UART
const int TX_BUF_SIZE = 25;          // Tamanho do buffer de escrita via UART
const int SEND_DATA_QUEUE_SIZE = 10; // Tamanho da fila de dados a serem enviados

const wifi_promiscuous_filter_t filt = { // Filtro dos pacotes a serem interceptados
    .filter_mask = WIFI_PROMIS_FILTER_MASK_MGMT | WIFI_PROMIS_FILTER_MASK_DATA};

int curChannel = 1; // Canal atual de operação

QueueHandle_t send_data_queue; // Fila de comunicação entre as tasks de sniffer e envio de dados

void init_uart(void);
int sendData(const char *logName, const char *data);
static void tx_task(void *arg);
static void rx_task(void *arg);
void sniffer(void *buf, wifi_promiscuous_pkt_type_t type);
void init_sniffer();
static void sniffer_task(void *arg);

void app_main(void)
{
    /*
    Função principal, responsavel por inicializar os modulos e criar todas as tasks
    */
    ESP_LOGI(TAG, "Iniciando...");

    init_uart();                                                                       // Configura a comunicação via UART
    init_sniffer();                                                                    // Configura e inicia o Sniffer
    xTaskCreate(rx_task, "uart_rx_task", 8192, NULL, configMAX_PRIORITIES - 2, NULL);  // Cria a task de recepção de dados via UART
    xTaskCreate(tx_task, "uart_tx_task", 4096, NULL, configMAX_PRIORITIES - 1, NULL);  // Cria a task de envio de dados via UART
    xTaskCreate(sniffer_task, "sniffer_task", 8192, NULL, configMAX_PRIORITIES, NULL); // Cria a task responsavel por alterar os canais do WiFi

    ESP_LOGI(TAG, "Rotina de inicialização concluida...");
}

void init_uart(void)
{
    /*
    Função responsavel por configurar e iniciar a UART
    */
    const uart_config_t uart_config = {
        // Struct com as confiugrações da UART
        .baud_rate = UART_BAUD_RATE,
        .data_bits = UART_DATA_8_BITS,
        .parity = UART_PARITY_DISABLE,
        .stop_bits = UART_STOP_BITS_1,
        .flow_ctrl = UART_HW_FLOWCTRL_DISABLE,
        .source_clk = UART_SCLK_DEFAULT,
    };
    uart_driver_install(UART_NUM, RX_BUF_SIZE * 2, 0, 0, NULL, 0);                    // Escolhe a UART a ser utilizada
    uart_param_config(UART_NUM, &uart_config);                                        // Configura a UART com base dos parametros do struct
    uart_set_pin(UART_NUM, TXD_PIN, RXD_PIN, UART_PIN_NO_CHANGE, UART_PIN_NO_CHANGE); // Configura os pinos da UART

    send_data_queue = xQueueCreate(SEND_DATA_QUEUE_SIZE, sizeof(char) * TX_BUF_SIZE); // Cria uma fila de envio de dados
}

int sendData(const char *logName, const char *data)
{
    /*
    Função responsavel por enviar dados via UART
    */
    const int len = strlen(data);                                // Obtenho o tamanho da string recebido
    const int txBytes = uart_write_bytes(UART_NUM_1, data, len); // Escreve os bytes na UART
    ESP_LOGI(logName, "Wrote %d bytes", txBytes);
    return txBytes; // Retorna a quantidade de bytes enviados
}

static void tx_task(void *arg)
{
    /*
    Task responsavel por realizar o envio dos dados via UART
    */
    static const char *TX_TASK_TAG = "TX_TASK";   // Crio uma tag de log especifica para os dados enviados
    esp_log_level_set(TX_TASK_TAG, ESP_LOG_INFO); // Seto com uma cor de log diferente
    char rxBuffer[TX_BUF_SIZE];                   // Aloco o buffer para enviar informações
    while (1)                                     // Repito infinitamente
    {
        if (xQueueReceive(send_data_queue, &(rxBuffer), portMAX_DELAY)) // Se tem algum dado na fila para ser enviado
        {
            sendData(TX_TASK_TAG, rxBuffer); // Envia os dados
        }
        vTaskDelay(50 / portTICK_PERIOD_MS); // Espera 50ms
    }
}

static void rx_task(void *arg)
{
    /*
    Task responsavel por ouvir e receber os dados da UART
    */
    static const char *RX_TASK_TAG = "RX_TASK";         // Crio uma tag de log especifica para os dados recebidos
    esp_log_level_set(RX_TASK_TAG, ESP_LOG_INFO);       // Seto com uma cor de log diferente
    uint8_t *data = (uint8_t *)malloc(RX_BUF_SIZE + 1); // Aloco o buffer para receber informações
    while (1)                                           // Repito infinitamente
    {
        const int rxBytes = uart_read_bytes(UART_NUM_1, data, RX_BUF_SIZE, 1000 / portTICK_PERIOD_MS); // Realiza a leitura do buffer da UART
        if (rxBytes > 0)                                                                               // Verifica se tem algo no buffer
        {
            data[rxBytes] = 0;                                           // Coloca a ultima posição do buffer após os dados como para sinalizar que ali e o fim da string
            ESP_LOGI(RX_TASK_TAG, "Read %d bytes: '%s'", rxBytes, data); // Imprime os dados
            ESP_LOG_BUFFER_HEXDUMP(RX_TASK_TAG, data, rxBytes, ESP_LOG_INFO);
        }
    }
    free(data); // Desaloca o buffer
}

void sniffer(void *buf, wifi_promiscuous_pkt_type_t type)
{
    /*
    Função responsavel por atual como callback de recepção dos pacotes interceptados.
    É aqui que os pacotes terminam depois de serem detectados
    */
    wifi_promiscuous_pkt_t *p = (wifi_promiscuous_pkt_t *)buf; // Converte o buffer recebido para um struct do tipo wifi_promiscuous_pkt_t
    int len = p->rx_ctrl.sig_len;                              // Obtem o tamanho do pacote recebido

    if (len <= 0) // Verifica se o tamanho e menor e igual a 0, caso se encaixe nesse padrão o pacote e invalido
    {
        ESP_LOGI(TAG, "Recebido 0");
        return; // Sai da função
    }

    char data[TX_BUF_SIZE]; // Aloca uma string para colocar os dados a serem enviados

    memset(data, 0, sizeof(data));

    sprintf(data, "%02X%02X%02X%02X%02X%02X,%d", p->payload[10],
            p->payload[11], p->payload[12], p->payload[13], p->payload[14], p->payload[15], p->rx_ctrl.rssi); // Coloca o mac do pacote detectado e o RSSI do mesmo separado or virgula

    xQueueSend(send_data_queue, (void *)data, (TickType_t)0); // Coloca os dados na fila para serem envidos via UART
}

void init_sniffer()
{
    /*
    Função responsavel por iniciar o wifi e configurar o mesmo no modo promiscous,
    desta forma permitindo o esp intercepetar pacotes de rede.
    */

    ESP_LOGI(TAG, "Inicinado WiFi no modo Promiscuous/Sniffer...");
    esp_err_t ret = nvs_flash_init();                                             // Inicia a memoria NVS (Responsavel por salvar valores de forma criptografada na flash)
    if (ret == ESP_ERR_NVS_NO_FREE_PAGES || ret == ESP_ERR_NVS_NEW_VERSION_FOUND) // Caso a NVS não esteja alocada, ou esteja cheia ele a formata
    {
        ESP_ERROR_CHECK(nvs_flash_erase()); // Formata a NVS
        ret = nvs_flash_init();             // Inicia a NVS
    }
    ESP_ERROR_CHECK(ret);

    wifi_init_config_t cfg = WIFI_INIT_CONFIG_DEFAULT(); // Struct de configuração do WiFi

    esp_wifi_init(&cfg);                                     // Inicia o WiFi
    esp_wifi_set_storage(WIFI_STORAGE_RAM);                  // Configura para o wifi utilizar a memoria ram para realizar qualquer tipo de armazenamento de dados
    esp_wifi_set_mode(WIFI_MODE_NULL);                       // Deixa o WiFi configurado em nenhum modo, pois ele não se conectara a nada e nem iniciara um hostpost
    esp_wifi_start();                                        // Starta o WiFi
    esp_wifi_set_promiscuous(true);                          // Ativa a interceptação de pacotes
    esp_wifi_set_promiscuous_filter(&filt);                  // Configura um filtro para interceptação dos pacotes
    esp_wifi_set_promiscuous_rx_cb(&sniffer);                // Seta a função responsavel por se comportar como callback
    esp_wifi_set_channel(curChannel, WIFI_SECOND_CHAN_NONE); // Configura o canal em que o WiFi ira operar
}

static void sniffer_task(void *arg)
{
    /*
    Função responsavel por realizar a troca de canais de forma automatica,
    varia de 1 a maxCh que e o canal maximo de acordo com a região
    */
    while (1) // Executa infinitamente
    {
        if (curChannel > maxCh) // Verica se o canal atual e maior que o limite
        {
            curChannel = 1; // Se sim reinicia a contagem
        }
        esp_wifi_set_channel(curChannel, WIFI_SECOND_CHAN_NONE); // Seta o novo canal

        vTaskDelay(pdMS_TO_TICKS(1000)); // Espera um segundo
        curChannel++;                    // Inclementa mais um no canal
    }
}