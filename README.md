# Sniffer Node

O Sniffer Node é um dispositivo responsável por realizar a interceptação física de pacotes, seu tratamento e encaminhamento para um dispositivo paralelo que envia as informações para a nuvem.

## Desenvolvedores:

- Autor: Julio Nunes Avelar.
- Entidade: Embarcações.
- Instituição: UNICAMP

## Hardware

### Principal:

- ESP32S2/ESP32/ESP32S3/ESP32C3
- Fonte de alimentação
- Case 3D impressa em ABS

### Opcional:

- RTC DS1307

## Software

- [ESP-IDF](https://docs.espressif.com/projects/esp-idf/en/latest/esp32/)

## Pré-requisitos:

- [ESP-IDF](https://docs.espressif.com/projects/esp-idf/en/latest/esp32/get-started/): É necessário ter o ESP-IDF para compilar e gravar o firmware na placa.

- Uma placa com o **ESP32**, **ESP32C3**, **ESP32S3** ou **ESP32S2**: Você precisará de uma placa de desenvolvimento ou uma placa profissional com um desses chips.

## Configurações:

Antes de gravar o firmware, é necessário realizar alguns ajustes no menu config.

Para invocar o ESP-IDF, utilize o seguinte comando:
```bash
get_idf
```

Após invocar o IDF, no diretório, escolha o MCU desejado. Para utilizar o ESP32, execute o comando:
```bash
idf.py set-target esp32
```
Se quiser utilizar o ESP32S2, utilize:
```bash
idf.py set-target esp32s2
```

O mesmo vale para os outros ESP32XX.

Se estiver utilizando a extensão do VS Code, essa opção está disponível no menu inferior (uma caixa com engrenagens e a opção "esp32", "esp32c3", "esp32s3" ou "esp32s2" ao lado).

## Gravação:

Antes de realizar a gravação, é necessário compilar o firmware. Você pode compilar usando o seguinte comando:
```bash
idf.py build
```

Após compilar o firmware, utilize o seguinte comando para gravá-lo:
```bash
idf.py flash
```

Se desejar utilizar o monitor serial para visualizar os logs, utilize o seguinte comando:
```bash
idf.py monitor
```

Para executar todas as etapas de uma vez, utilize o comando:
```bash
idf.py build flash monitor
```

Também é possível realizar essas etapas por meio da extensão do ESP-IDF para o VS Code, selecionando a opção "ESP-IDF build, flash and monitor".

## Organização do projeto:

O projeto está organizado em 2 pastas principais:

- main: Contém o arquivo principal do programa.

- docs: Toda a documentação dos componentes e do projeto está presente nessa pasta.

## Dúvidas e Bugs:

Para qualquer dúvida, bug na aplicação ou sugestão, utilize o menu "issues" no GitLab.